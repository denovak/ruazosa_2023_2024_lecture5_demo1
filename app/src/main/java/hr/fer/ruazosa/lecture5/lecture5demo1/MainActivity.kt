package hr.fer.ruazosa.lecture5.lecture5demo1

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import hr.fer.ruazosa.lecture5.lecture5demo1.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.startLongRunningOperationButton.setOnClickListener {

            Thread {
                for (i in 1..10) {
                    Thread.sleep(1000)
                    runOnUiThread {
                        binding.operationStatusTextView.text = i.toString()
                    }

                }
            }.start()

        }

        binding.sayHelloButton.setOnClickListener {
            val toastMessage = Toast.makeText(applicationContext, "Hello world", Toast.LENGTH_LONG)
            toastMessage.show()

        }

    }



}